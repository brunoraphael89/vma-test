# Before running the application

**Environment Variables (Mongo and Secret)**

- Create a `.env` file with the variables shown on the file `.env.example`


# Instructions to run the application

**Server**

- On the project folder run yarn to install the dependencies:
- `yarn`

- Start the server:
- `yarn dev:server`

**Tests**

- On the project folder run:
- `yarn test`


# API Calls

 - Start Creating a user in POST /users

 - Authenticate to receive a token in POST /sessions

 - Put the token on Authorization Header: `Bearer {TOKEN}`

 - An Insomnia Collection will be available with all the API calls in the folder `insomnia_collection`
