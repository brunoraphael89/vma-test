import { sign } from 'jsonwebtoken';
import { jwt } from '@config/auth';
import { injectable, inject } from 'tsyringe';
import { AppError } from '@shared/errors/AppError';

import { ObjectId } from 'mongodb';
import { IUsersRepository } from '../repositories/IUsersRepository';
import { IHashProvider } from '../providers/HashProvider/models/IHashProvider';

interface IRequest {
  email: string;
  password: string;
}

interface IResponse {
  token: string;
}
@injectable()
export class AuthenticateUserService {
  constructor(
    @inject('UsersRepository') private usersRepository: IUsersRepository,
    @inject('HashProvider') private hashProvider: IHashProvider,
  ) { }

  public async execute({ email, password }: IRequest): Promise<IResponse> {
    const user = await this.usersRepository.findByEmail(email);

    if (!user) {
      throw new AppError('Email or Password are incorrect', 401);
    }

    const passwordMatched = await this.hashProvider.compareHash(
      password,
      user.password,
    );

    if (!passwordMatched) {
      throw new AppError('Email or Password are incorrect', 401);
    }

    const { secret, expires_in } = jwt;

    const token = sign({}, secret, {
      subject: new ObjectId(user.id).toString(),
      expiresIn: expires_in,
    });

    return {
      token,
    };
  }
}
