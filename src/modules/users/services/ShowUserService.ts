import { AppError } from '@shared/errors/AppError';
import { injectable, inject } from 'tsyringe';
import { User } from '../infra/typeorm/schemas/User';
import { IUsersRepository } from '../repositories/IUsersRepository';

interface IRequest {
  id: string;
}

@injectable()
export class ShowUserService {
  constructor(
    @inject('UsersRepository') private usersRepository: IUsersRepository,
  ) { }

  public async execute({ id }: IRequest): Promise<User> {
    const user = await this.usersRepository.findById(id);

    if (!user) {
      throw new AppError('User not found');
    }

    return user;
  }
}
