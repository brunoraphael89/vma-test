import 'reflect-metadata';
import { AppError } from '@shared/errors/AppError';
import { AuthenticateUserService } from './AuthenticateUserService';
import { FakeUsersRepository } from '../repositories/fakes/FakeUsersRepository';
import { FakeHashProvider } from '../providers/HashProvider/fakes/FakeHashProvider';

let fakeUserRepository: FakeUsersRepository;
let fakeHashProvider: FakeHashProvider;
let authenticateUser: AuthenticateUserService;

describe('AuthenticateUser', () => {
  beforeEach(() => {
    fakeUserRepository = new FakeUsersRepository();
    fakeHashProvider = new FakeHashProvider();

    authenticateUser = new AuthenticateUserService(
      fakeUserRepository,
      fakeHashProvider,
    );
  });

  it('should be able to authenticate', async () => {
    await fakeUserRepository.create({
      name: 'Bruno',
      email: 'bruno@example',
      password: 'Senha123',
      address: 'Street Bruno',
      age: 31,
    });

    const response = await authenticateUser.execute({
      email: 'bruno@example',
      password: 'Senha123',
    });

    expect(response).toHaveProperty('token');
  });

  it('should not be able to authenticate with non existing user', async () => {
    await expect(
      authenticateUser.execute({
        email: 'bruno@example',
        password: 'Senha123',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to authenticate with wrong password', async () => {
    await fakeUserRepository.create({
      name: 'Bruno',
      email: 'bruno@example',
      password: 'Senha123',
      address: 'Street Bruno',
      age: 31,
    });

    await expect(
      authenticateUser.execute({
        email: 'bruno@example',
        password: 'wrong-password',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
