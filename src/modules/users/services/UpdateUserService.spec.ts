import 'reflect-metadata';
import { AppError } from '@shared/errors/AppError';
import { FakeHashProvider } from '@modules/users/providers/HashProvider/fakes/FakeHashProvider';
import { UpdateUserService } from './UpdateUserService';
import { FakeUsersRepository } from '../repositories/fakes/FakeUsersRepository';

let fakeUserRepository: FakeUsersRepository;
let fakeHashProvider: FakeHashProvider;
let updateUser: UpdateUserService;

describe('UpdateUser', () => {
  beforeEach(() => {
    fakeUserRepository = new FakeUsersRepository();
    fakeHashProvider = new FakeHashProvider();

    updateUser = new UpdateUserService(fakeUserRepository, fakeHashProvider);
  });

  it('should be able to update the user information', async () => {
    const user = await fakeUserRepository.create({
      name: 'Bruno',
      email: 'bruno@example',
      password: '123456',
      address: 'Street Bruno',
      age: 31,
    });

    const updatedUser = await updateUser.execute({
      id: user.id.toString(),
      name: 'Bruno Updated',
      email: 'bruno-updated@example',
      address: 'Street Bruno',
      age: 31,
    });

    expect(updatedUser.name).toBe('Bruno Updated');
    expect(updatedUser.email).toBe('bruno-updated@example');
  });

  it('should not be able to update the user information from non-existing user', async () => {
    expect(
      updateUser.execute({
        id: 'non-existing-user-id',
        name: 'Test',
        email: 'test@example.com',
        address: 'adress',
        age: 32,
      }),
    ).rejects.toBeInstanceOf(AppError);
  });

  it('should not be able to change to another user email', async () => {
    await fakeUserRepository.create({
      name: 'Bruno',
      email: 'bruno@example',
      password: '123456',
      address: 'Street Bruno',
      age: 31,
    });

    const user = await fakeUserRepository.create({
      name: 'Bruno 2',
      email: 'bruno2@example',
      password: '123456',
      address: 'Street Bruno',
      age: 31,
    });

    await expect(
      updateUser.execute({
        id: user.id.toString(),
        name: 'Bruno 2',
        email: 'bruno@example',
        address: 'Street Bruno',
        age: 31,
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
