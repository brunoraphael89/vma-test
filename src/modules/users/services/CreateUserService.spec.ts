import 'reflect-metadata';
import { AppError } from '@shared/errors/AppError';
import { CreateUserService } from './CreateUserService';
import { FakeUsersRepository } from '../repositories/fakes/FakeUsersRepository';
import { FakeHashProvider } from '../providers/HashProvider/fakes/FakeHashProvider';

let fakeUserRepository: FakeUsersRepository;
let fakeHashProvider: FakeHashProvider;
let createUser: CreateUserService;

describe('CreateUser', () => {
  beforeEach(() => {
    fakeUserRepository = new FakeUsersRepository();
    fakeHashProvider = new FakeHashProvider();

    createUser = new CreateUserService(fakeUserRepository, fakeHashProvider);
  });

  it('should be able to create a new user', async () => {
    const user = await createUser.execute({
      name: 'Bruno',
      email: 'bruno@example',
      password: '123456',
      address: 'Street Bruno',
      age: 31,
    });

    expect(user).toHaveProperty('id');
  });

  it('should not be able to create a new user with same email from another', async () => {
    await createUser.execute({
      name: 'Bruno',
      email: 'bruno@example',
      password: '123456',
      address: 'Street Bruno',
      age: 31,
    });

    await expect(
      createUser.execute({
        name: 'Bruno',
        email: 'bruno@example',
        password: '123456',
        address: 'Street Bruno',
        age: 31,
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
