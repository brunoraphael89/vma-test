import 'reflect-metadata';
import { AppError } from '@shared/errors/AppError';
import { ObjectId } from 'mongodb';
import { DeleteUserService } from './DeleteUserService';
import { FakeUsersRepository } from '../repositories/fakes/FakeUsersRepository';

let fakeUserRepository: FakeUsersRepository;
let deleteUser: DeleteUserService;

describe('DeleteUser', () => {
  beforeEach(() => {
    fakeUserRepository = new FakeUsersRepository();

    deleteUser = new DeleteUserService(fakeUserRepository);
  });
  it('should be able to delete the user', async () => {
    const user = await fakeUserRepository.create({
      name: 'Bruno',
      email: 'bruno@example',
      password: '123456',
      address: 'Street Bruno',
      age: 31,
    });

    const isDeleted = await deleteUser.execute({
      id: new ObjectId(user.id).toString(),
    });

    expect(isDeleted).toBe(true);
  });

  it('should not be able to delete the a non existing user', async () => {
    await expect(
      deleteUser.execute({
        id: new ObjectId().toString(),
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
