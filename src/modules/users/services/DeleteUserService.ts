import { AppError } from '@shared/errors/AppError';
import { injectable, inject } from 'tsyringe';
import { IUsersRepository } from '../repositories/IUsersRepository';

interface IRequest {
  id: string;
}

@injectable()
export class DeleteUserService {
  constructor(
    @inject('UsersRepository') private usersRepository: IUsersRepository,
  ) { }

  public async execute({ id }: IRequest): Promise<boolean> {
    const user = await this.usersRepository.findById(id);

    if (!user) {
      throw new AppError('User not found');
    }

    const isDeleted = await this.usersRepository.delete(id);

    return isDeleted;
  }
}
