import { AppError } from '@shared/errors/AppError';
import { injectable, inject } from 'tsyringe';
import { IHashProvider } from '@modules/users/providers/HashProvider/models/IHashProvider';
import { ObjectId } from 'mongodb';
import { User } from '../infra/typeorm/schemas/User';
import { IUsersRepository } from '../repositories/IUsersRepository';

interface IRequest {
  id: string;
  name: string;
  age: number;
  email: string;
  address: string;
}

@injectable()
export class UpdateUserService {
  constructor(
    @inject('UsersRepository') private usersRepository: IUsersRepository,
    @inject('HashProvider') private hashProvider: IHashProvider,
  ) { }

  public async execute({
    id,
    age,
    name,
    email,
    address,
  }: IRequest): Promise<User> {
    const user = await this.usersRepository.findById(id);

    if (!user) {
      throw new AppError('User not found');
    }

    const userWithUpdatedEmail = await this.usersRepository.findByEmail(email);

    if (
      userWithUpdatedEmail &&
      new ObjectId(userWithUpdatedEmail.id).toString() !== id
    ) {
      throw new AppError('Email already in use');
    }

    user.name = name;
    user.age = age;
    user.email = email;
    user.address = address;

    return this.usersRepository.save(user);
  }
}
