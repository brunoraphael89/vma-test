import { injectable, inject } from 'tsyringe';
import { AppError } from '@shared/errors/AppError';
import { User } from '../infra/typeorm/schemas/User';
import { IUsersRepository } from '../repositories/IUsersRepository';
import { IHashProvider } from '../providers/HashProvider/models/IHashProvider';

interface IRequest {
  name: string;
  age: number;
  email: string;
  password: string;
  address: string;
}

@injectable()
export class CreateUserService {
  constructor(
    @inject('UsersRepository') private usersRepository: IUsersRepository,
    @inject('HashProvider') private hashProvider: IHashProvider,
  ) { }

  public async execute({
    address,
    age,
    password,
    email,
    name,
  }: IRequest): Promise<User> {
    const userExists = await this.usersRepository.findByEmail(email);

    if (userExists) {
      throw new AppError('Email address already exists');
    }

    const hashedPassword = await this.hashProvider.generateHash(password);

    const user = await this.usersRepository.create({
      address,
      age,
      email,
      name,
      password: hashedPassword,
    });

    return user;
  }
}
