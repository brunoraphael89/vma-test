import 'reflect-metadata';
import { AppError } from '@shared/errors/AppError';
import { ObjectId } from 'mongodb';
import { ShowUserService } from './ShowUserService';
import { FakeUsersRepository } from '../repositories/fakes/FakeUsersRepository';

let fakeUserRepository: FakeUsersRepository;
let showUser: ShowUserService;

describe('ShowUser', () => {
  beforeEach(() => {
    fakeUserRepository = new FakeUsersRepository();

    showUser = new ShowUserService(fakeUserRepository);
  });
  it('should be able to show the user information', async () => {
    const user = await fakeUserRepository.create({
      name: 'Bruno',
      email: 'bruno@example',
      password: '123456',
      address: 'Street Bruno',
      age: 31,
    });

    const profile = await showUser.execute({
      id: new ObjectId(user.id).toString(),
    });

    expect(profile.name).toBe('Bruno');
    expect(profile.email).toBe('bruno@example');
  });

  it('should not be able to show the user information from non-existing user', async () => {
    expect(
      showUser.execute({
        id: 'non-existing-user-id',
      }),
    ).rejects.toBeInstanceOf(AppError);
  });
});
