import { Request, Response } from 'express';
import { container } from 'tsyringe';
import { CreateUserService } from '@modules/users/services/CreateUserService';
import { UpdateUserService } from '@modules/users/services/UpdateUserService';
import { ShowUserService } from '@modules/users/services/ShowUserService';
import { DeleteUserService } from '@modules/users/services/DeleteUserService';
import { classToClass } from 'class-transformer';

export class UsersController {
  async create(request: Request, response: Response): Promise<Response> {
    const { address, age, password, email, name } = request.body;

    const createUser = container.resolve(CreateUserService);

    const user = await createUser.execute({
      address,
      age,
      password,
      email,
      name,
    });

    return response.json(classToClass(user));
  }

  public async show(request: Request, response: Response): Promise<Response> {
    const { id } = request.user;
    const showUser = container.resolve(ShowUserService);

    const user = await showUser.execute({ id });

    return response.json(classToClass(user));
  }

  public async update(request: Request, response: Response): Promise<Response> {
    const { id } = request.user;
    const { name, age, address, email } = request.body;

    const updateUser = container.resolve(UpdateUserService);

    const user = await updateUser.execute({
      id,
      name,
      age,
      address,
      email,
    });

    return response.json(classToClass(user));
  }

  public async delete(request: Request, response: Response): Promise<Response> {
    const { id } = request.user;
    const deleteUser = container.resolve(DeleteUserService);

    await deleteUser.execute({ id });

    return response.status(204).send();
  }
}
