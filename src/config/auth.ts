const jwt = {
  secret: process.env.APP_SECRET || 'default',
  expires_in: '1d',
};

export { jwt };
