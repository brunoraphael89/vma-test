module.exports = {
  name: 'default',
  type: process.env.DB_TYPE,
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  database: process.env.DB_DATABASE,
  useUnifiedTopology: true,
  entities: ['./**/modules/**/infra/typeorm/schemas/*.{js,ts}'],
};

